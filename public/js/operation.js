function operation(searchValue = '') {
    let container = $('#pagination');
    let itemPerPage = 6;
    var apiUrl = "https://world.openfoodfacts.org/cgi/search.pl?search_terms=" + searchValue + "&search_simple=1&action=process&page_size=" + itemPerPage + "&json=1"

    container.pagination({
        dataSource: apiUrl,
        locator: 'products',
        totalNumber: 100,
        pageSize: itemPerPage,
        showPrevious: false,
        showNext: false,
        className: 'paginationjs-theme-blue paginationjs-small',
        ajax: {
            beforeSend: function () {
                container.prev().html('Loading data...');
            }
        },
        callback: function (response, pagination) {
            var dataHtml = "";
            // в этом пагинаторе косячно работает вставка след данных из запроса
            // поэтому тут пришлось повторно делать GET запрос, чтоб данные адекватно отображались.
            $.get(apiUrl + "&page=" + pagination.pageNumber, function (data) {
                $.each(data.products, function (index, item) {
                    dataHtml += "\
                                    <div class='col'>\
                                        <div class='card h-100'>\
                                        <div class='text-center'>\
                                            <img src='" + item.image_front_small_url + "' width='40%' class='img-thumbnail' alt='" + item.product_name + "'>\
                                        </div>\
                                        <div class='card-body'>\
                                            <p class='card-text'>" + item.product_name + "</p>\
                                        </div>\
                                        <div class='card-footer'>\
                                            <a href='#!' id='saveProduct' class='btn btn-outline-primary bt-sm' data-id='"+item._id+"' >save</a>\
                                        </div>\
                                        </div>\
                                    </div>\
                                ";
                });
                container.prev().html(dataHtml);
                prepareData(data);
            });

        },

    });
}

function prepareData(data)
{
    $('.card-footer a').on('click', function(event) {
        var id = event.target.getAttribute('data-id');
        var getSpecificProduct = data.products.find(el => el._id == id);
        var prepareData = {
            '_id' : getSpecificProduct._id,
            'product_name' : getSpecificProduct.product_name,
            'image_url' : getSpecificProduct.image_front_small_url,
            'categories' : getSpecificProduct.categories_tags
        };

        saveProduct(prepareData)
    });
}

function saveProduct(data) {
    var notify = $('#notify');
    $.ajax({
        type: "POST",
        url: "http://appsmart.local/api/v1/product",
        data: data,
        success: function(response) {
            notify.fadeIn('slow');
            notify.append(response.message);
            setTimeout(function(){
                notify.fadeOut(25);
                notify.html("");
            }, 1000);
        },
    }).fail(function(jqXHR) {
        alert(jqXHR.responseJSON.message);
    })
}
