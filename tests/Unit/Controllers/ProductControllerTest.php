<?php

namespace Tests\Unit\Controllers;

use Tests\TestCase;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function testUpdateOrCreateSuccess()
    {
        $payload = [
            '_id' => 1,
            'image_url' => \Str::random(10),
            'product_name' => \Str::random(10),
            'categories' => ['cake', 'bakery products', 'desserts']
        ];

        $response = $this->json('POST', route('product.createOrUpdate'), $payload);
        $response->assertJson(['status' => true, 'message' => __('Product successfully created or updated.')]);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('products', ['id' => $payload['_id']]);
    }

    public function testUpdateOrCreateWithOutData()
    {
        $response = $this->json('POST', route('product.createOrUpdate'));
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(
            [
                "message" => "The given data was invalid.",
                "errors" => [
                    "_id" => ["The  id field is required."],
                    "image_url" => ["The image url field is required."],
                    "product_name" => ["The product name field is required."],
                    "categories" => ["The categories field is required."],
                ]
            ]
        );
    }

}
