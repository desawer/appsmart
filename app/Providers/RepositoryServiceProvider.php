<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Products\Repositories\{
    ProductRepository,
    Interfaces\IProductRepository,
};

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IProductRepository::class,ProductRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
