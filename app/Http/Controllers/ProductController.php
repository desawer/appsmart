<?php

namespace App\Http\Controllers;

use App\Services\Products\ProductService;
use App\Http\Requests\Products\UpdateOrCreateRequest;
use Illuminate\Http\{JsonResponse, Response};

class ProductController extends Controller
{
    private ProductService $productService;

    public function __construct(
        ProductService $productService
    )
    {
        $this->productService = $productService;
    }

    public function updateOrCreate(UpdateOrCreateRequest $request): JsonResponse
    {
        try {
            $id = $request->input('_id');
            $convertedData = $request->only('image_url', 'product_name');
            $convertedData['id'] = $id;
            $convertedData['categories'] = json_encode($request->input('categories'));

            $this->productService->createOrUpdate(['id' => $id], $convertedData);
            return response()->json(['status' => true, 'message' => __('Product successfully created or updated.')], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['status' => false , 'message' => $exception->getMessage(), 'trace' => $exception->getTrace()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
