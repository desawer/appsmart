<?php

namespace App\Models;

use Illuminate\Database\Eloquent\{
    Model,
    Factories\HasFactory
};

class Product extends Model
{
    use HasFactory;

    protected $guarded = [];
}
