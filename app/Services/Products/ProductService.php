<?php

namespace App\Services\Products;

use App\Services\Products\Repositories\Interfaces\IProductRepository;

class ProductService
{
    private IProductRepository $productRepository;

    public function __construct(
        IProductRepository $productRepository
    )
    {
        $this->productRepository = $productRepository;
    }

    public function createOrUpdate(array $attributes, array $values): void
    {
        $this->productRepository->createOrUpdate($attributes, $values);
    }
}
