<?php

namespace App\Services\Products\Repositories\Interfaces;

interface IProductRepository
{
    public function createOrUpdate(array $attributes, array $values): void;
}
