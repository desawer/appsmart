<?php

namespace App\Services\Products\Repositories;

use \App\Models\Product;
use \Illuminate\Database\Eloquent\Builder;
use App\Services\Products\Repositories\Interfaces\IProductRepository;

class ProductRepository implements IProductRepository
{
    protected function getQueryBuilder(): Builder
    {
        return Product::query();
    }

    public function createOrUpdate(array $attributes, array $values): void
    {
        $this->getQueryBuilder()->updateOrCreate($attributes, $values);
    }
}
