<div class="col">
    <div class="row">
        <div class="col"></div>
        <div class="col">
            <form>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Product search | <small>please write key words
                            and Enter press.</small></label>
                    <input type="text" class="form-control" id="product_name">
                </div>
            </form>
        </div>
        <div class="col"></div>
    </div>
    <div class="row">
        <div class="col"></div>
        <div class="col">
            <div id="notify" class="alert alert-success collapse" role="alert"></div>
            <div class='row row-cols-1 row-cols-md-3 g-4 mb-3 h-100'>
                <div class="data-container m-3"></div>
            </div>
            <div id="pagination" class="mb-5"></div>
        </div>
        <div class="col"></div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $('.form-control').keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                var searchValue = $('#product_name').val();
                // function import from operation.js
                operation(searchValue);
            }
        });
    });
</script>
